import pandas as pd
import matplotlib.pyplot as plt

datasales = pd.read_csv("data.csv", header=0, sep=",")

datasales.plot(x ='Store_ID', y='Total_Sales', kind='scatter')

plt.show()