import pandas as pd
import numpy as np

datasales = pd.read_csv("data.csv", header=0, sep=",")

cv = np.std(datasales) / np.mean(datasales)

print(cv)